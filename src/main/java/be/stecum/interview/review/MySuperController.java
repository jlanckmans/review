package be.stecum.interview.review;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MySuperController {

    /**
     * Fetch a map with CustomHeaders
     *
     * @param request
     * @return map with headers and value
     */
    Map<String, String> fetchHeaderMap(HttpServletRequest request) {
        Map<String, String> headerMapper = new ConcurrentHashMap<String, String>();
        Enumeration<String> names = request.getHeaderNames();

        while (names.hasMoreElements()) {
            String name = (String)names.nextElement();
            Enumeration<String> values = request.getHeaders(name);
            if (values != null) {
                while (values.hasMoreElements()) {
                    String value = (String) values.nextElement();
                    if (CustomHeader.MY_CUSTUM_ID.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.MY_CUSTUM_ID.httpHeaderValue(), value);
                    }
                    if (CustomHeader.SOME_REFERENCE.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.SOME_REFERENCE.httpHeaderValue(), value);
                    }
                    if (CustomHeader.USER_REF.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.USER_REF.httpHeaderValue(), value);
                    }
                    if (CustomHeader.EMPLOYEE.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.EMPLOYEE.httpHeaderValue(), value);
                    }
                }
            }
        }

        return headerMapper;
    }

    Map<String, String> fetchHeaderMapBetterImplementation(HttpServletRequest request) {
        Map<String, String> headerMapper = new ConcurrentHashMap<>();
        Enumeration<String> names = request.getHeaderNames();

        for (String name = names.nextElement(); names.hasMoreElements(); ) {
            for (CustomHeader customHeader : CustomHeader.values()) {
                if (customHeader.httpHeaderValue.equalsIgnoreCase(name)) {
                    Enumeration<String> values = request.getHeaders(name);
                    for (String value = values.nextElement(); values.hasMoreElements(); ) {
                        headerMapper.put(customHeader.httpHeaderValue, value);
                    }
                }
            }
        }
        return headerMapper;
    }

    enum CustomHeader {
        MY_CUSTUM_ID("X-Cust-Id"),
        SOME_REFERENCE("X-Some-Ref"),
        USER_REF("Y-UserRef"),
        EMPLOYEE("X-EmplId");

        private final String httpHeaderValue;

        CustomHeader(String httpHeaderValue) {
            this.httpHeaderValue = httpHeaderValue;
        }

        public String httpHeaderValue() {
            return httpHeaderValue;
        }

    }
}
